import React, { Component } from 'react';
import Cookies from 'js-cookie';
import { Route, Redirect, Switch } from 'react-router-dom';

class AppRouter extends Component {

  constructor (props) {
      super (props)
      this.state = {
        is_logged:false,
      }

      this.handleLogin = this.handleLogin.bind(this)

  }

   handleLogin() {

    let gmt = Cookies.get('gmt')

    if(gmt !== undefined){
      return(<Redirect from="/" to="/dashboard" /> )
    }else{
      return(<Redirect from="/" to="/login" />)
    }
      
  }

  render() {
      return (
              <Switch>
                  <Route exact path="/">
                    {this.handleLogin()}
                  </Route>
              </Switch>
      );
  }
}

export default AppRouter;
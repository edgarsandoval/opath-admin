import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import { Menu, ExitToApp, PostAdd, Edit, Delete, TableChart, PersonAdd, ChevronLeft, ChevronRight, MenuBook, ExpandLess, ExpandMore  } from '@material-ui/icons';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ListSubheader from '@material-ui/core/ListSubheader';
import {  NavLink } from "react-router-dom";
import Cookies from 'js-cookie';
import ApartmentIcon from '@material-ui/icons/Apartment';
import DevicesIcon from '@material-ui/icons/Devices';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function logout(){

  Cookies.remove('gmt');
  Cookies.remove('gmu');
  window.location.reload();
}

export default function MiniDrawer() {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [openz,setOpens] = React.useState(false);
  const [openChecador,setOpenC] = React.useState(false);


  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleClick = () => {
    setOpens(!openz);
  };

  

  const handleClickC = () => {
    setOpenC(!openChecador);
  };

  

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
        style={{backgroundColor:'#393d47'}}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <Menu />
          </IconButton>
          <Typography variant="h6" style={{color:'#fff'}} noWrap>
            GrupoMatch
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRight /> : <ChevronLeft />}
          </IconButton>
        </div>
        <Divider />
          <List>
              <ListItem button onClick={handleClick}>
                <ListItemIcon>
                  <DevicesIcon />
                </ListItemIcon>
                <ListItemText primary="Matchmakers" />
                {openz ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse in={openz} timeout="auto" unmountOnExit>
                <List component="div" 
                  subheader={
                  <ListSubheader style={{position:"relative"}} component="div" id="nested-list-subheader">
                    Usuarios
                  </ListSubheader>}>
                  <NavLink to="/ShowAuthor" style={{color:'black',textDecoration:'none'}}>
                    <ListItem button className={classes.nested}>
                      <IconButton >
                        <TableChart />
                      </IconButton>
                      <ListItemText primary="Ver Matchmakers" />
                    </ListItem>
                  </NavLink>
                  <NavLink to="/AddAuthor" style={{color:'black',textDecoration:'none'}}>
                    <ListItem button className={classes.nested}>
                      <IconButton >
                        <PersonAdd />
                      </IconButton>
                      <ListItemText primary="Agregar Matchmakers" />
                    </ListItem>
                  </NavLink>
                  <NavLink to="/EditAuthor" style={{color:'black',textDecoration:'none'}}>
                  <ListItem button className={classes.nested}>
                    <IconButton >
                      <Edit />
                    </IconButton>
                    <ListItemText primary="Editar Matchmaker" />
                  </ListItem>
                  </NavLink>
                  <ListItem button className={classes.nested}>
                    <IconButton >
                      <Delete />
                    </IconButton>
                    <ListItemText primary="Eliminar Matchmaker" />
                  </ListItem>
                </List>
                <List component="div" disablePadding 
                  subheader={
                  <ListSubheader component="div" id="nested-list-subheader">
                    Artículos
                  </ListSubheader>}>
                  <ListItem button className={classes.nested}>
                    <IconButton >
                      <TableChart />
                    </IconButton>
                    <ListItemText primary="Ver artículos" />
                  </ListItem>
                  <ListItem button className={classes.nested}>
                    <IconButton >
                      <PostAdd />
                    </IconButton>
                    <ListItemText primary="Agregar artículo" />
                  </ListItem>
                  <ListItem button className={classes.nested}>
                    <IconButton >
                      <Edit />
                    </IconButton>
                    <ListItemText primary="Editar artículo" />
                  </ListItem>
                  <ListItem button className={classes.nested}>
                    <IconButton >
                      <Delete />
                    </IconButton>
                    <ListItemText primary="Eliminar artículo" />
                  </ListItem>
                </List>
              </Collapse> 
            </List>
        <Divider />


              {/* Menu checador */}
          <Divider />
            <List>
              <ListItem button onClick={handleClickC}>
                <ListItemIcon>
                  <ApartmentIcon />
                </ListItemIcon>
                <ListItemText primary="Proyectos" />
                {openChecador ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse in={openChecador} timeout="auto" unmountOnExit>
                <List component="div">
                  <NavLink to="/Registros" style={{color:'black',textDecoration:'none'}}>
                    <ListItem button className={classes.nested}>
                      <IconButton >
                        <TableChart />
                      </IconButton>
                      <ListItemText primary="Ver proyectos" />
                    </ListItem>
                  </NavLink>
                  <NavLink to="/Likes" style={{color:'black',textDecoration:'none'}}>
                    <ListItem button className={classes.nested}>
                      <IconButton >
                        <TableChart />
                      </IconButton>
                      <ListItemText primary="Ver Likes" />
                    </ListItem>
                  </NavLink>
                  <NavLink to="/AgregarRegistros" style={{color:'black',textDecoration:'none'}}>
                    <ListItem button className={classes.nested}>
                      <IconButton >
                        <PersonAdd />
                      </IconButton>
                      <ListItemText primary="Agregar registro" />
                    </ListItem>
                  </NavLink>
                  <NavLink to="/EditarRegistros" style={{color:'black',textDecoration:'none'}}>
                  <ListItem button className={classes.nested}>
                    <IconButton >
                      <Edit />
                    </IconButton>
                    <ListItemText primary="Editar registro" />
                  </ListItem>
                  </NavLink>
                 {/*  <ListItem button className={classes.nested}>
                    <IconButton >
                      <Delete />
                    </IconButton>
                    <ListItemText primary="Eliminar registro" />
                  </ListItem> */}
                </List>
              </Collapse> 
            </List>
          <Divider />








        <List>
            <ListItem button onClick={() => {
              logout()
            }}>
              <ListItemIcon> <ExitToApp /></ListItemIcon>
              <ListItemText primary='Log out' />
            </ListItem>
        </List>
      </Drawer>
      
    </div>
  );
}
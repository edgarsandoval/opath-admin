import React, { Component } from 'react';
import { GoogleMap, withScriptjs, withGoogleMap, Marker, TrafficLayer, KmlLayer } from "react-google-maps";
const axios = require('axios');


 
     

class Rutas extends Component {

    constructor() {
        super();

        this.state = {
            data:[]
        };
     
        
    }

    getData = () => {

        axios.get('http://localhost:8080/proyectos')
         .then(async (response) => {
            console.log(response);
           this.setState({
             data: response.data.resp
           });
     
         })
         .catch(function (error) {
           // handle error
           console.log(error);
         })
         .then(function () {
           // always executed
         });
  
         
     
       }

       componentDidMount(){
           this.getData()
       }

    

    Map = () =>{

        return(
            <GoogleMap
                    defaultZoom={12}
                    defaultCenter={{ 
                        lat: 20.6736,
                        lng: -103.344
                    }}
                >
                
                
                    {
                        this.state.data.map((element) => (

                            <Marker 
                                key={element.id}
                                position={{ lat: parseFloat(element.latitud), lng: parseFloat(element.longitud) }}
                            />
                        ))
                    }
                   
                    <TrafficLayer autoUpdate />
                </GoogleMap>
        )
    
    }


    
    
    render(){

        const  WrappedMap = withScriptjs(withGoogleMap(this.Map))

        return(

            <div style={{width:'100vw',height:'100vh'}} >
                <WrappedMap 
                
                    googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyDYCE6ing3huVfqwFZ6T8AZnTuWlBeYLT0"
                    loadingElement={<div style={{ height : '100%' }} > </div>}
                    containerElement={<div style={{ height: `100vh` }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                
                ></WrappedMap>
            </div>
        )
    }

    }

export default Rutas;

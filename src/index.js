import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Home from './App';
import Dashboard from './Dashboard';
import Login from './Login';
import AddAuthor from './Blog/AddAuthor';
import EditAuthor from './Blog/EditAuthor';
import ShowAuthor from './Blog/ShowAuthor';
import Likes from './proyectos/likes';
import Rutas from './Rutas/verRutas';


import AgregarRegistros from './Checador/AgregarRegistros';
import EditarRegistros from './Checador/EditarRegistros';
import Registros from './Checador/Registros';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route } from 'react-router-dom';

const App = () => (

    <BrowserRouter>

        <Route exact path='/'><Home/></Route>
        <Route path='/dashboard' ><Dashboard/></Route>
        <Route path='/login' ><Login/></Route>
        <Route path='/AddAuthor' ><AddAuthor/></Route>
        <Route path='/ShowAuthor' ><ShowAuthor/></Route>
        <Route path='/EditAuthor' ><EditAuthor/></Route>
        <Route path='/Registros' ><Registros/></Route>
        <Route path='/AgregarRegistros' ><AgregarRegistros/></Route>
        <Route path='/EditarRegistros' ><EditarRegistros/></Route>
        <Route path='/Likes' ><Likes/></Route>
        <Route path='/Rutas' ><Rutas/></Route>




    </BrowserRouter>

)

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

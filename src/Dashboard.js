import React, { Component } from 'react';
import './App.css';
import Menu from './Menu'
import Cookies from 'js-cookie';
import { Route, Redirect, Switch } from "react-router-dom";
import validar from '../src/Blog/validar'

class Dashboard extends Component {

  constructor() {
    super();
    this.state = {};
  }

  componentDidMount(){
  }

  render() {
    let Cookie = Cookies.get('gmt')
    if(Cookie === undefined){

        return(
          <Switch>
              <Route exact path="/dashboard">
                  <Redirect from="/dashboard" to="/login" /> 
              </Route>
          </Switch>
        )

    }else{
        return (
            <div>
                <Menu></Menu>
                <div className="wrapper">
                <div className="one"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum incidunt labore tempora laboriosam? Omnis sint amet deleniti autem tenetur aspernatur, natus ad at quis itaque incidunt sunt? Hic, labore quo.</div>
                <div className="two">Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum incidunt labore tempora laboriosam? Omnis sint amet deleniti autem tenetur aspernatur, natus ad at quis itaque incidunt sunt? Hic, labore quo.</div>
                </div>
            </div>
        );
    }

  }
  
}



export default Dashboard;
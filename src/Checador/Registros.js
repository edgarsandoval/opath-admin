import React, { Component } from 'react';
import '../Blog/css-blog/showauthor.css'
import Menu from '../Menu'
import Cookies from 'js-cookie';
import { Route, Redirect, Switch } from "react-router-dom";
import axios from 'axios';
import 'antd/dist/antd.css'; 
import { Table, Input, Button  } from 'antd';
import validar from '../Blog/validar'
import Highlighter from "react-highlight-words";
import SearchIcon from '@material-ui/icons/Search';



class Registros extends Component {

  constructor() {
    super();
    this.state = {

        data:[],
        onSearch:'',
        searchText: '',
        searchedColumn: '',

    };

    this._request = this._request.bind(this)


  }

  /* 
  6405 = matchmakers
  7198 = balbuena  
  */

  
 getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon={<SearchIcon />}
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => <SearchIcon style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  _request(){

        let gmt = Cookies.get('gmt');
        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            token: gmt,
        }

        axios.get('http://155.138.160.88:8080/checador', {headers})
        .then(response => {
    
            let objetoRespuesta = JSON.parse(response.request.response).resp
            this.setState({

                data: objetoRespuesta

            })
            console.log(this.state.data)

            objetoRespuesta.forEach(element => {
                console.log(element.author_name)
            });

        })
        .catch(  (error) => {
          console.log(error)
        })

  }

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  componentDidMount(){
    this._request()
    validar()
  }


  render() {
    const columns = [
        {
          title: 'Registro',
          dataIndex: 'registro',
          key: 'registro',
          ...this.getColumnSearchProps('registro'),
        },
        {
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre',
            ...this.getColumnSearchProps('nombre'),
        },
        {
            title: 'Entrada',
            dataIndex: 'entrada',
            key: 'entrada',
        },
        {
            title: 'Salida',
            dataIndex: 'salida',
            key: 'salida',
        },
        {
            title: 'Dia',
            dataIndex: 'dia',
            key: 'dia',
            ...this.getColumnSearchProps('dia'),
        },
        {
            title: 'Periodo',
            dataIndex: 'periodo',
            key: 'periodo',
            ...this.getColumnSearchProps('periodo'),
        },

          
    ]

    let Cookie = Cookies.get('gmt')

    
    if(Cookie === undefined){
      
        return(
          <Switch>
              <Route exact path="/Registros">
                  <Redirect from="/Registros" to="/login" /> 
              </Route>
          </Switch>
        )

    }else{

        return (
            <div>
                <Menu></Menu>
                <div className="wrappers">
                    <div className="ones">

                        <Table columns={columns} dataSource={this.state.data}>
                        </Table>

                    </div>
                </div>
            </div>
        );
    }

  }
  
}



export default Registros;
import React, { Component } from 'react';
import '../Blog/css-blog/showauthor.css'
import Menu from '../Menu.js';
import Cookies from 'js-cookie';
import { Route, Redirect, Switch } from "react-router-dom";
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import { Select} from 'antd';
import validar from '../Blog/validar';
import { DatePicker } from 'antd';

const { Option } = Select;

class AddAuthor extends Component {

  constructor() {
    super();
    this.state = {
        registro: '',
        nombre: '',
        entrada: '',
        salida:'',
        dia:'',
        periodo: '',
        datos:[],
        success: false,
        mensaje:'',
        faltaInfo:false


    };

    this.selectRol = this.selectRol.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    
  }

    onChangeHandler=event=>{

        this.setState({
            imagen:  event.target.files[0]
        })

    }

    componentDidMount(){

        validar()
        this.consultaNombres()
    }

    consultaNombres(){

        let gmt = Cookies.get('gmt');
        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            token: gmt,
        }
    
        axios.get('http://155.138.160.88:8080/users', {headers})
        .then(response => {
    
            let datos = response.data.resp
            this.setState({
                datos
            })
    
        })
        .catch(  (err) => {
          console.log(err)
        })
    
      }


   async handleSubmit(){

        let registro = this.state.registro
        let nombre = this.state.nombre
        let entrada = this.state.entrada
        let salida = this.state.salida
        let dia = this.state.dia
        let periodo = this.state.periodo

    

        let data = {
            registro,
            nombre,
            entrada,
            salida,
            dia,
            periodo
        }

        
        console.log(data)
       
        let gmt = Cookies.get('gmt');

        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            token: gmt,
        }
        

      axios.post('http://155.138.160.88:8080/checador', data, {headers})
        .then(response => {

            alert('registro agregado correctamente')

        })
        .catch(  (error) => {

            console.log(error)

        })

  }

async selectRol(rol){

    await this.setState({
        rol: rol.target.value
    })

}

  render() {
    let Cookie = Cookies.get('gmt')

    if(Cookie === undefined){
      
        return(
          <Switch>
              <Route exact path="/AgregarRegistros">
                  <Redirect from="/AgregarRegistros" to="/login" /> 
              </Route>
          </Switch>
        )

    }else{
        return (
            <div>
                <Menu></Menu>
                <div className="wrapperz">
                    <div className="onez">
                        <ValidatorForm
                            ref="form"
                            onSubmit={this.handleSubmit}
                            onError={errors => this.handleSubmit(errors)}
                            style={{width:900}}
                        >
                        <TextValidator
                            label="Registro"
                            onChange={(registro) => {
                                this.setState({
                                    registro: registro.target.value
                                })
                            }}
                            name="name"
                            value={this.state.registro}
                            validators={['required']}
                            errorMessages={['Es necesario este campo.']}
                            style={{marginTop:20,width:250,marginRight:20}}
                        />


                        <Select
                            style={{ width: 200,marginTop:35,marginRight:10 }}
                            placeholder="Seleccion un nombre"
                            onChange={(value) => {
                                this.setState({
                                    nombre: value
                                })
                            }}
                            allowClear
                        >

                            {this.state.datos.map(item => {
                                return (
                                    <Option key={item} value={item.author_name}>{item.author_name}</Option>
                                );
                            })}
                        </Select>

                        <TextValidator
                            label="Entrada"
                            onChange={(entrada) => {
                                this.setState({
                                    entrada: entrada.target.value
                                })
                            }}
                            name="Entrada"
                            value={this.state.entrada}
                            validators={['required']}
                            errorMessages={['this field is required']}
                            style={{marginTop:20,width:250,marginRight:20}}
                        />

                        <TextValidator
                            label="Salida"
                            onChange={(salida) => {
                                this.setState({
                                    salida: salida.target.value
                                })
                            }}
                            name="salida"
                            value={this.state.salida}
                            validators={['required']}
                            errorMessages={['this field is required']}
                            style={{marginTop:20,width:250,marginRight:20,}}
                        />

                        <DatePicker format='YYYY-MM-DD' style={{marginTop:35,marginRight:10}} onChange={(dia)=> {
                            console.log(dia)
                            this.setState({
                                dia:dia._d
                            })

                        }} />
                        
                        <Select
                            showSearch
                            style={{ width: 200,marginTop:35, }}
                            placeholder="Selecciona un mes"
                            onChange={(value)=>{
                                this.setState({
                                    periodo:value
                                })
                            }}
                        >
                            <Option value="enero">Enero</Option>
                            <Option value="febrero">Febrero</Option>
                            <Option value="marzo">Marzo</Option>
                            <Option value="abril">Abril</Option>
                            <Option value="mayo">Mayo</Option>
                            <Option value="junio">Junio</Option>
                            <Option value="julio">Julio</Option>
                            <Option value="agosto">Agosto</Option>
                            <Option value="septiembre">Septiembre</Option>
                            <Option value="octubre">Octubre</Option>
                            <Option value="noviembre">Noviembre</Option>
                            <Option value="diciembre">Diciembre</Option>
                        </Select>

                        
                        
                       <br></br> <Button style={{marginTop:30}} variant="contained" color="primary" type="submit">Crear</Button>
                    </ValidatorForm>
                    
                </div>
            </div>
        </div>
        );
    }

  }
  
}



export default AddAuthor;
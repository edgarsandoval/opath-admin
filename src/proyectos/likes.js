import React, { Component } from 'react';
import '../Blog/css-blog/AddAuthor.css';
import Menu from '../Menu';
import Cookies from 'js-cookie';
import { Route, Redirect, Switch } from "react-router-dom";
import axios from 'axios';
import validar from '../Blog/validar';
import { Bar,Line  } from 'react-chartjs-2';

class EditAuthor extends Component {

    constructor() {
        super();
        this.state = {

            chardata:{

                labels:['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Noviembre','Diciembre']
                ,datasets:[
                    {

                        label: 'My First dataset',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: 'rgba(75,192,192,0.4)',
                        borderColor: 'rgba(75,192,192,1)',
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: 'rgba(75,192,192,1)',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 2,
                        pointRadius: 5,
                        pointHitRadius: 10,
                        data: [65, 59, 80, 81, 56, 55, 40]

                    }
            ]
            }

        };


    }
  

    componentDidMount(){

        validar()
        
    }

  render() {
    let Cookie = Cookies.get('gmt')

    if(Cookie === undefined){
      
        return(
          <Switch>
              <Route exact path="/Likes">
                  <Redirect from="/Likes" to="/login" /> 
              </Route>
          </Switch>
        )

    }else{
        return (
        <div>
            <Menu></Menu>
            <div className="wrappers">
                <div style={{width:600,marginTop:'10%'}}>

                    <Line  width={200} height={150} ref={this.chartReference} data={this.state.chardata}   />

                </div>
            </div>

        </div>
        );
    }

  }
  
}



export default EditAuthor;
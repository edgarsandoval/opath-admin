import React, { Component } from 'react';
import './css-blog/AddAuthor.css';
import Menu from '../Menu.js';
import Cookies from 'js-cookie';
import { Route, Redirect, Switch } from "react-router-dom";
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import { Select} from 'antd';
import validar from './validar';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import { Alert, AlertTitle } from '@material-ui/lab';
import CloseIcon from '@material-ui/icons/Close';

const { Option } = Select;

class AddAuthor extends Component {

  constructor() {
    super();
    this.state = {
        rol: '',
        nombre: '',
        email: '',
        password:'',
        imagen:'',
        error: false,
        success: false,
        mensaje:'',
        faltaInfo:false


    };

    this.selectRol = this.selectRol.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    
  }

    onChangeHandler=event=>{

        this.setState({
            imagen:  event.target.files[0]
        })

    }

    componentDidMount(){

        validar()

    }


    handleSubmit(){
        
        let name = this.state.nombre
        let email = this.state.email
        let password = this.state.password
        let image = this.state.imagen
        let role = this.state.rol




        var jsonData = {

            name,
            email,
            password,
            role

        };

        let imagenFile = image
       
        const data = new FormData();
        data.append("document", imagenFile);
        data.append("body", JSON.stringify(jsonData));

        let gmt = Cookies.get('gmt')

        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            token: gmt,
        }

        axios.post('http://localhost:8080/users', data, {headers})
        .then(response => {

            let respuesta = response
            let error = respuesta.data.error
            console.log(respuesta)
            console.log(error)
            let mensaje = respuesta.data.err.message
            console.log(mensaje)

            switch (error) {
                case 1:
                    this.setState({
                        mensaje,
                        error:true
                    })
                    break;
                case 2:
                    this.setState({
                        mensaje,
                        error:true
                    })
                    break;
                case 3:
                    this.setState({
                        mensaje,
                        error:true
                    })
                    break;
                case 4:
                    this.setState({
                        mensaje,
                        error:true
                    })
                    break;
                case 5:
                    this.setState({
                        mensaje,
                        error:true
                    })
                break;
                case 6:
                    this.setState({
                        mensaje,
                        success:true,
                        nombre:'',
                        email:'',
                        password:'',
                        imagen:'',
                        rol:''
                    })
                    setTimeout(function() {
                        window.location.reload();
                    }, 1500);
                    
                break;
            
                default:
                    break;
            }

        })
        .catch(  (error) => {

            console.log(error)

        })

  }



  render() {
    let Cookie = Cookies.get('gmt')

    if(Cookie === undefined){
      
        return(
          <Switch>
              <Route exact path="/AddAuthor">
                  <Redirect from="/AddAuthor" to="/login" /> 
              </Route>
          </Switch>
        )

    }else{
        return (
            <div>
                <Menu></Menu>
                <div className="wrapperz">
                    <div className="onez">
                    <Collapse in={this.state.error}>
                        <Alert
                            severity="error"
                            action={
                                <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    this.setState({error:false});
                                }}
                                >
                                    <CloseIcon fontSize="inherit" />
                                </IconButton>
                            }
                            >
                            <AlertTitle>Error</AlertTitle>
                            {this.state.mensaje}
                        </Alert>
                    </Collapse>
                    <Collapse in={this.state.success}>
                        <Alert
                            severity="success"
                            action={
                                <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    this.setState({success:false});
                                }}
                                >
                                    <CloseIcon fontSize="inherit" />
                                </IconButton>
                            }
                            >
                            <AlertTitle>Bienvenido Matchmaker!</AlertTitle>
                            {this.state.mensaje}
                        </Alert>
                    </Collapse>
                    <Collapse in={this.state.faltaInfo}>
                        <Alert
                            severity="error"
                            action={
                                <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    this.setState({faltaInfo:false});
                                }}
                                >
                                    <CloseIcon fontSize="inherit" />
                                </IconButton>
                            }
                            >
                            <AlertTitle>Error</AlertTitle>
                            Ingresa toda la información necesaria por favor!
                        </Alert>
                    </Collapse>
                        <ValidatorForm
                            ref="form"
                            onSubmit={this.handleSubmit}
                            onError={errors => this.handleSubmit(errors)}
                            style={{width:900}}
                        >
                        <TextValidator
                            label="Nombre"
                            onChange={(nombre) => {
                                this.setState({
                                   nombre: nombre.target.value
                                })
                            }}
                            name="name"
                            value={this.state.nombre}
                            validators={['required']}
                            errorMessages={['Es necesario este campo.']}
                            style={{marginTop:20,width:250,marginRight:20}}
                        />
                        <TextValidator
                            label="Email"
                            onChange={(email) => {
                                this.setState({
                                   email: email.target.value
                                })
                            }}
                            name="email"
                            value={this.state.email}
                            validators={['required', 'isEmail']}
                            errorMessages={['this field is required', 'email is not valid']}
                            style={{marginTop:20,width:250,marginRight:20}}
                        />
                        <TextValidator
                            label="Password"
                            onChange={(password) => {
                                this.setState({
                                   password: password.target.value
                                })
                            }}
                            name="password"
                            type="password"
                            value={this.state.password}
                            validators={['required']}
                            errorMessages={['this field is required']}
                            style={{marginTop:20,width:250,marginRight:20}}
                        />
                        <Select
                            showSearch
                            style={{ width: 250,marginRight:20 }}
                            placeholder="Rol"
                            onChange={(rol) => {
                                this.setState({
                                    rol
                                })
                            }}
                           
                            filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            <Option value={'ADMIN_ROLE'}>Admin</Option>
                            <Option value={'USER_ROLE'}>Usuario</Option>
                        </Select>
                        
                        <input
                            style={{marginTop:50}}
                            type="file"
                            validators={['required']}
                            errorMessages={['this field is required']}
                            onChange={this.onChangeHandler}
                        />
                       <br></br> <Button style={{marginTop:30}} variant="contained" color="primary" type="submit">Crear</Button>
                    </ValidatorForm>
                    
                </div>
            </div>
        </div>
        );
    }

  }
  
}



export default AddAuthor;
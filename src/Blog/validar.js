
import Cookies from 'js-cookie';
import axios from 'axios';

function _validar(){

    let gmt = Cookies.get('gmt');

    axios.post('http://155.138.160.88:8080/validarToken', {token:gmt})

    .then(response => {

      let message = response.data.err.message
      if (message === false ){

        Cookies.remove('gmt');

        window.location.reload();

      }

    })
    .catch(  (error) => {

      console.log(error)

    })
  }

  export default _validar;
import React, { Component } from 'react';
import './css-blog/AddAuthor.css';
import Menu from '../Menu';
import Cookies from 'js-cookie';
import { Route, Redirect, Switch } from "react-router-dom";
import axios from 'axios';
import validar from './validar';

class EditAuthor extends Component {

    constructor() {
        super();
        this.state = {
            rol: '',
            nombre: '',
            email: '',
            password:'',
            imagen:'',
        };

        this._traerUsuarios = this._traerUsuarios.bind(this)
        this._editUser = this._editUser.bind(this)

    }
    


    _traerUsuarios(){

        let gmt = Cookies.get('gmt');
        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            token: gmt,
        }

        axios.get('http://155.138.160.88:8080/users', {headers})
        .then(response => {

            let objetoRespuesta = JSON.parse(response.request.response).resp
            this.setState({

                data: objetoRespuesta

            })
            console.log(this.state.data)

            /* objetoRespuesta.forEach(element => {
                console.log(element.author_name)
            }); */

        })
        .catch(  (error) => {
          console.log(error)
        })

    }

    _editUser(){

        let name = this.state.nombre
        let email = this.state.email
        let password = this.state.password
        let image = this.state.imagen
        let role = this.state.rol

        if(image || role === ""){

          return this.setState({

                faltaInfo: true

            })

        }

        var jsonData = {

            name,
            email,
            password,
            role

        };

        let imagenFile = image
       
        const data = new FormData();
        data.append("document", imagenFile);
        data.append("body", JSON.stringify(jsonData));

        /* let gmt = Cookies.get('gmt') */

        /* var headers = {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            token: gmt,
        } */


    }

    componentDidMount(){

        this._traerUsuarios()
        
    }

  render() {
    let Cookie = Cookies.get('gmt')

    if(Cookie === undefined){
      
        return(
          <Switch>
              <Route exact path="/EditAuthor">
                  <Redirect from="/EditAuthor" to="/login" /> 
              </Route>
          </Switch>
        )

    }else{
        return (
        <div>
            <Menu></Menu>

        </div>
        );
    }

  }
  
}



export default EditAuthor;
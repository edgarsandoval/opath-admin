import React, { Component } from 'react';
import './css-blog/AddAuthor.css';
import Menu from '../Menu.js';
import Cookies from 'js-cookie';
import { Route, Redirect, Switch } from "react-router-dom";
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import { Select} from 'antd';


class AddAuthor extends Component {

  constructor() {
    super();
    this.state = {

        nombre: '',



    }; 

    this.handleSubmit = this.handleSubmit.bind(this)
    
  }

  componentDidMount(){
      this.handleSubmit()
  }


   async handleSubmit(){
        
        let name = 'Ruta 1'
        
        let gmt = Cookies.get('gmt')

        var header = {
            'Authorization':'Bearer ' +  gmt,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        var postData = {
            name
          };


      let resp = await axios.post('https://opath.edgarsandoval.mx/routes')
        console.log('respuiesta');
        console.log(resp);

  }



  render() {
    let Cookie = Cookies.get('gmt')

    if(Cookie === undefined){
      
        return(
          <Switch>
              <Route exact path="/AddAuthor">
                  <Redirect from="/AddAuthor" to="/login" /> 
              </Route>
          </Switch>
        )

    }else{
        return (
            <div>
                <Menu></Menu>
                <div className="wrapperz">
                    <div className="onez">
                    
                        <ValidatorForm
                            ref="form"
                            onSubmit={this.handleSubmit}
                            onError={errors => this.handleSubmit(errors)}
                            style={{width:900}}
                        >
                        <TextValidator
                            label="Nombre Ruta"
                            onChange={(nombre) => {
                                this.setState({
                                   nombre: nombre.target.value
                                })
                            }}
                            name="name"
                            value={this.state.nombre}
                            validators={['required']}
                            errorMessages={['Es necesario este campo.']}
                            style={{marginTop:20,width:250,marginRight:20}}
                        />
                       <br></br> <Button style={{marginTop:30}} variant="contained" color="primary" type="submit">Crear</Button>
                    </ValidatorForm>
                    
                </div>
            </div>
        </div>
        );
    }

  }
  
}



export default AddAuthor;
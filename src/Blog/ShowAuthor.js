import React, { Component } from 'react';
import './css-blog/showauthor.css'
import Menu from '../Menu.js'
import Cookies from 'js-cookie';
import { Route, Redirect, Switch } from "react-router-dom";
import axios from 'axios';
import 'antd/dist/antd.css'; 
import { Table, Divider, Tag  } from 'antd';
import validar from './validar'

const { Column } = Table;


class ShowAuthor extends Component {

  constructor() {
    super();
    this.state = {

        data:[]

    };

    this._request = this._request.bind(this)


  }

  _request(){

        let gmt = Cookies.get('gmt');
        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            token: gmt,
        }

        axios.get('http://155.138.160.88:8080/users', {headers})
        .then(response => {

            let objetoRespuesta = JSON.parse(response.request.response).resp
            this.setState({

                data: objetoRespuesta

            })

        })
        .catch(  (error) => {
          console.log(error)
        })

  }

  componentDidMount(){
    this._request()
  }


  render() {
    
    let Cookie = Cookies.get('gmt')

    if(Cookie === undefined){
      
        return(
          <Switch>
              <Route exact path="/ShowAuthor">
                  <Redirect from="/ShowAuthor" to="/login" /> 
              </Route>
          </Switch>
        )

    }else{
        return (
            <div>
                <Menu></Menu>
                <div className="wrappers">
                    <div className="ones">
                        <Table dataSource={this.state.data}>
                            <Column title="ID" dataIndex="id_author" key="id_author" />
                            <Column title="Nombre" dataIndex="author_name" key="author_name" />
                            <Column title="Email" dataIndex="author_email" key="author_email" />
                            <Column
                            title="Role"
                            dataIndex="author_role"
                            key="tags"
                            render={author_role => (
                                <span>
                                
                                    <Tag color="Green">
                                    {author_role}
                                    </Tag>
                               
                                </span>
                            )}
                            />
                            <Column
                            title="Action"
                            key="action"
                            render={(text, record) => (
                                <span>
                                <a href="/">Editar</a>
                                <Divider type="vertical" />
                                <a href="/">Desactivar</a>
                                </span>
                            )}
                            />
                        </Table>
                    </div>
                </div>
            </div>
        );
    }

  }
  
}



export default ShowAuthor;
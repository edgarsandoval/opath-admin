
import React, { Component } from 'react';
import './Login.css';
import InputAdornment from '@material-ui/core/InputAdornment';
import { Email, Lock} from '@material-ui/icons';
import Button from '@material-ui/core/Button';
import Collapse from '@material-ui/core/Collapse';
import { Alert, AlertTitle } from '@material-ui/lab';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import axios from 'axios';
import Cookies from 'js-cookie';
import { Route, Redirect, Switch } from "react-router-dom";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
          isGoing: true,
          email: '',
          password: '',
          redirect: false,
          setOpen: true,
          open:false
        };
    
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleEmail = this.handleEmail.bind(this)
        this.handlePass = this.handlePass.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)


      }



  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  handleEmail(event) {
    let valor = event.target.value
      this.setState({
          email : valor
      }) 
  }

  handlePass(event) {
        let valor = event.target.value
        this.setState({
            password : valor
        }) 
    }

    async handleSubmit(event){
        let email = this.state.email
        let password = this.state.password

        var postData = {
          email,
          password
        };

        console.log(postData);

    let resp = await axios.post('https://opath.edgarsandoval.mx/login',postData,)
        console.log(resp);
          // handle success
          if(resp.status === 200){
            let usuario = resp.data.data.user
            let token = resp.data.data.token;
            Cookies.set('gmt', token, { expires: 7, path: '/'});
            Cookies.set('gmu', usuario, { expires: 7, path: '/'});
  
            this.setState({
              redirect:true
            }) 
          }
    }

 

  render() {
        const { redirect } = this.state;
        let Cookie = Cookies.get('gmt')

        if (redirect) {
          return <Redirect to='/dashboard'/>;
        }

        if(Cookie !== undefined){
            return(
              <Switch>
                  <Route exact path="/login">
                      <Redirect from="/login" to="/dashboard" /> 
                  </Route>
              </Switch>
            )

        }else{

        return (
            
          <div className="Login">
            <img src={require('./logos/match.png')} alt="logoMatch.png" className="Logo" />
            <ValidatorForm
                ref="form"
                onSubmit={this.handleSubmit}
                onError={errors => this.handleSubmit(errors)}
            >
              <Collapse in={this.state.open}>
                <Alert
                  severity="error"
                  action={
                    <IconButton
                      aria-label="close"
                      color="inherit"
                      size="small"
                      onClick={() => {
                        this.setState({open:false});
                      }}
                    >
                      <CloseIcon fontSize="inherit" />
                    </IconButton>
                  }
                >
                  <AlertTitle>Error</AlertTitle>
                  Correo electronico y / o contraseña incorrecto.
                </Alert>
              </Collapse>
            <div className="inputDiv">
                <TextValidator
                    className="inputs"
                    value={this.state.email}
                    id="input-with-icon"
                    label="Correo"
                    onChange={this.handleEmail}
                    validators={['required', 'isEmail']}
                    errorMessages={['this field is required', 'email is not valid']}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                            <Email />
                            </InputAdornment>
                        ),
                    }}
                />
            </div>
            
            <div className="inputDiv">
                <TextValidator
                    className="inputs"
                    id="input-with-icon-textfield"
                    label="Contraseña"
                    type="password"
                    value={this.state.password}
                    onChange={this.handlePass}
                    validators={['required']}
                    errorMessages={['this field is required']}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                            <Lock />
                            </InputAdornment>
                        ),
                    }}
                />
            </div>
            <div className="inputDiv">
                <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                >
                    Login
                </Button>
            </div>
            
            </ValidatorForm>
            <div className="checkbox">
                <label className="labelCheckbox">
                    <input
                        name="isGoing"
                        type="checkbox"
                        checked={this.state.isGoing}
                        onChange={this.handleInputChange} 
                    />Recordarme
                </label>
                <a href="/"><label className="labelForgot">
                    Olvide mi contraseña
                </label></a>
            </div>
            
          </div>

        );
      }
    }
  
}



export default App;
